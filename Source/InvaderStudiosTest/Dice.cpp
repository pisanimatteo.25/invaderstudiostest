// Fill out your copyright notice in the Description page of Project Settings.


#include "Dice.h"
#include "Components/PrimitiveComponent.h"
#include "Math/UnrealMathUtility.h"
// Sets default values
ADice::ADice()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADice::BeginPlay()
{
	Super::BeginPlay();
	GetComponents<UStaticMeshComponent>(Components);
	DynMaterial = UMaterialInstanceDynamic::Create(MaterialDice, this);
	Components[0]->SetMaterial(0, DynMaterial);
}

// Called every frame
void ADice::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ADice::Launch()
{
	int dirX = FMath::RandRange(-100, 100);
	int dirY = FMath::RandRange(-100, 100);
	int dirZ = FMath::RandRange(-100, 100);
	if (Components.IsValidIndex(0))
	{
		Components[0]->AddImpulse(FVector::UpVector * Impulse, NAME_None, false);
		Components[0]->AddTorqueInRadians(FVector(dirX, dirY, dirZ) * ImpulseRotation, NAME_None, true);
	}
}

//
int ADice::GetFaceValue()
{
	if (canBeLaunched)
	{
		float dotFwd = FVector::DotProduct(GetActorForwardVector(), FVector::UpVector);
		if (dotFwd > 0.99f) return 1;
		if (dotFwd < -0.99f) return 6;

		float dotRight = FVector::DotProduct(GetActorRightVector(), FVector::UpVector);
		if (dotRight > 0.99f) return 3;
		if (dotRight < -0.99f) return 4;

		float dotUp = FVector::DotProduct(GetActorUpVector(), FVector::UpVector);
		if (dotUp > 0.99f) return 5;
		if (dotUp < -0.99f) return 2;

		return 0;
	}
	return 0;
}

void ADice::SetTexture()
{
	if(canBeLaunched)
		DynMaterial->SetTextureParameterValue("Texture", T_Dice);
	else
		DynMaterial->SetTextureParameterValue("Texture", T_DiceDisabled);
}