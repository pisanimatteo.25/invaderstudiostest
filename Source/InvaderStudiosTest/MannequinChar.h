// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MannequinChar.generated.h"

UENUM(BlueprintType)
enum class StatusPlayer : uint8 {
	Active = 0,
	Inactive = 1
};

UCLASS()
class INVADERSTUDIOSTEST_API AMannequinChar : public ACharacter
{
	GENERATED_BODY()

    bool inRun = false;
public:
	// Sets default values for this character's properties
	AMannequinChar();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable")
		class AInteractable* Interactable;

	UFUNCTION(BlueprintCallable)
		void InteractPressed();

	//Status player 
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		StatusPlayer status = StatusPlayer::Active;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float RunSpeed = 375.0f;

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    void Run();
};
