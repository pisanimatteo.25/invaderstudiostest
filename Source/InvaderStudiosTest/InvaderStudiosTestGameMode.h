// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "InvaderStudiosTestGameMode.generated.h"

UCLASS(minimalapi)
class AInvaderStudiosTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AInvaderStudiosTestGameMode();
};



