// Copyright Epic Games, Inc. All Rights Reserved.

#include "InvaderStudiosTestGameMode.h"
#include "UObject/ConstructorHelpers.h"

AInvaderStudiosTestGameMode::AInvaderStudiosTestGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/InvaderStudiosAssets/Mannequin/BP_MannequinChar"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
