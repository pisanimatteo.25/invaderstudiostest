// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Dice.generated.h"

class UStaticMeshComponent;
UCLASS()
class INVADERSTUDIOSTEST_API ADice : public AActor
{
	GENERATED_BODY()
	
private:
	TArray<UStaticMeshComponent*> Components;
public:	
	// Sets default values for this actor's properties
	ADice();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Attesa per lanciare il dado
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Launch")
		bool canBeLaunched = true;
	
	
	//Material Dice
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Material")
		UMaterialInterface* MaterialDice;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Material")
		UMaterialInstanceDynamic* DynMaterial;

	//Textures Dice
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Texture")
		UTexture* T_DiceDisabled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Texture")
		UTexture* T_Dice;

	//Vars potenza di lancio e rotazione
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Impulse")
		float Impulse = 500.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Impulse")
		float ImpulseRotation = 15.0f;

	//Lancio dado
	UFUNCTION(BlueprintCallable)
		void Launch();

	//Valore faccia 
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Value Up Face")
		int valueDice;
	UFUNCTION(BlueprintCallable)
		int GetFaceValue();
	UFUNCTION(BlueprintCallable)
		void SetTexture();
};
