// Fill out your copyright notice in the Description page of Project Settings.


#include "MannequinChar.h"
#include "Interactable.h"
#include "GameFramework/CharacterMovementComponent.h"
// Sets default values
AMannequinChar::AMannequinChar()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    GetCharacterMovement()->MaxWalkSpeed = RunSpeed * 0.5f;

}

// Called when the game starts or when spawned
void AMannequinChar::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AMannequinChar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMannequinChar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMannequinChar::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMannequinChar::MoveRight);

    PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AMannequinChar::InteractPressed);
    PlayerInputComponent->BindAction("Run", IE_Pressed, this, &AMannequinChar::Run);

}

void AMannequinChar::InteractPressed()
{
    if (Interactable && Interactable->GetIsInteractable() && status == StatusPlayer::Active)
    {
	    status = StatusPlayer::Inactive;
    }
}

void AMannequinChar::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AMannequinChar::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AMannequinChar::Run()
{
	if (inRun)
		GetCharacterMovement()->MaxWalkSpeed = RunSpeed * 0.5f;
	
    else
        GetCharacterMovement()->MaxWalkSpeed = RunSpeed;
    inRun = !inRun;
	GetCharacterMovement()->bOrientRotationToMovement = inRun;
}